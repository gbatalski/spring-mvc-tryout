package de.batalski;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class SayerTest {

	@Inject
	private Sayer sayer;

	@Test
	public void test() {
		assertEquals("Hello", sayer.say("Hello"));
	}

}
