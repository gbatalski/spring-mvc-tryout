package de.batalski;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {
	@Bean
	public Sayer createSayer() {
		return new Sayer();

	}
}
