package de.batalski.rest;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.batalski.Sayer;

@RestController
public class SayerRest {
	@Inject
	private Sayer delegate;
	@RequestMapping("/greeting")
	public String say(@RequestParam(name = "txt",required=true, defaultValue = "Default" ) String somethingToSay) {
		return delegate.say(somethingToSay);

	}
}
