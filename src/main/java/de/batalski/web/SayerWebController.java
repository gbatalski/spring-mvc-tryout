package de.batalski.web;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.batalski.Sayer;

@Controller
public class SayerWebController {
	@Inject
	Sayer sayer;

	@RequestMapping("/say")
	public String saySomething(@RequestParam("text") String text, Model model) {
		model.addAttribute("text", sayer.say(text));
		return "say";

	}

}
